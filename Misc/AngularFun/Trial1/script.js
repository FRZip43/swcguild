//js code

(function() {

	var app = angular.module("githubViewer", []);

	var MainController = function($scope) {

		var person = {
			firstName: "David",
			lastName: "Hume",
			imgSrc: "http://www.philosophybasics.com/photos/hume.jpg"
		};

		$scope.message = "Hello Angular!";
		$scope.person = person;
	};

	var GitHubController = function($scope, $http) {

		var onUserComplete = function(response) {
			$scope.user = response.data;
		};

		var onError = function(reason) {
			$scope.error = "Could not fetch the user";
		};

		$http.get("https://api.github.com/users/robconery")
			 .then(onUserComplete, onError);
	};

	app.controller("MainController", ["$scope", MainController]);
	app.controller("GitHubController", ["$scope", "$http", GitHubController]);

}());