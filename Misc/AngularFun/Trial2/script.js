//js code

(function() {

	var app = angular.module("githubViewer", []);

	var MainController = function($scope) {

		var person = {
			firstName: "David",
			lastName: "Hume",
			imgSrc: "http://www.philosophybasics.com/photos/hume.jpg"
		};

		$scope.message = "Hello Angular!";
		$scope.person = person;
	};

	var GitHubController = function($scope, $http) {

		var onUserComplete = function(response) {
			$scope.user = response.data;
			$http.get($scope.user.repos_url)
				 .then(onRepos, onError);
		};

		var onRepos = function(response) {
			$scope.repos = response.data;
		};

		var onError = function(reason) {
			$scope.error = "Could not fetch the data";
		};

		$scope.search = function(username) {
			$http.get("https://api.github.com/users/" + username)
				 .then(onUserComplete, onError);
		};

		$scope.username = "angular";
		$scope.message = "Playing with AngularJS!";
		$scope.repoSortOrder = "language";
	};

	app.controller("MainController", ["$scope", MainController]);
	app.controller("GitHubController", ["$scope", "$http", GitHubController]);

}());