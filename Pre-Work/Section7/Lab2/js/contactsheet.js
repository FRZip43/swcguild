/*Section 7 Lab 2 01/25/17
js for contact page check */

function checkForm() {

	var phone = document.forms["contactForm"]["phone"].value;
	var email = document.forms["contactForm"]["email"].value;
	var reason = document.forms["contactForm"]["reason"].value;
	var addInfo = document.forms["contactForm"]["additionalInformation"].value;

	if (phone == "" && email == "") {
		alert("Please enter your phone number or email address.");
		return false;
	} else if (reason == "OTHER" && addInfo == "") {
		alert("Please enter additional information");
		return false;
	} else if (document.getElementById("checkM").checked == false && document.getElementById("checkT").checked == false && document.getElementById("checkW").checked == false && document.getElementById("checkH").checked == false && document.getElementById("checkF").checked == false) {
		alert("Please select at-least one best contact day");
		return false;
	}
}