function findMax(arrayToSort) {
	var maxValue = arrayToSort[0];
	for (var arrayPosition = 0; arrayPosition < arrayToSort.length; arrayPosition++){
		if (arrayToSort[arrayPosition] > maxValue){
			maxValue = arrayToSort[arrayPosition];
		}
	}
	return maxValue;	
}